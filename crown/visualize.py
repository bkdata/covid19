import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

plt.style.use('seaborn-whitegrid')


def readLatest():
    daily = pd.read_csv('./data/daily/20200401_RIVM_Epi_Sit.csv')
    daily["Datum"] = pd.to_datetime(daily["Datum"], format="%Y-%m-%d")
    daily["Datum Publicatie"] = pd.to_datetime(daily["Datum Publicatie"],
                                               format="%Y-%m-%d")

    return daily


def plotLineZiekenhuisopnames():
    f, ax = plt.subplots(figsize=(10, 10))
    latestData = readLatest()
    ziekenhuisopnames = np.array(latestData['Ziekenhuisopnames'])
    timestamps = np.array(latestData['Datum'])
    ax.plot(timestamps, ziekenhuisopnames)

    plt.show()


__all__ = ['readLatest']
