import pandas as pd


def createHistoric():
    daily = pd.read_csv("./data/daily/20200401_RIVM_Epi_Sit.csv")
    return daily


__all__ = [createHistoric]